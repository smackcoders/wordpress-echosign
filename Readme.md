=== Echo Sign ===
Contributors: smackcoders
Donate link: http://www.smackcoders.com/donate.html
Tags: Echo Sign, digitallysignpdf, howtoelectronicallysign, Echo Signfree, electronicsignaturespdf, howtosignelectronically, adobeEcho Sign, Echo Signadobe, signdocumentselectronically, electronicsign, digitalsignaturespdf, digitallysigndocuments, onlinecontractsigning, users, ecommerce, admin, customers, pdf, shortcode
Requires at least: 4
Tested up to: 4.3.1
Stable tag: 1.1
Version: 1.1
Author: smackcoders
Author URI: http://profiles.wordpress.org/smackcoders/

License: GPLv2 or later

Echo Sign plugin to integrate Adobe Echo Sign features to wordpress. 

== Description ==

Echo Sign plugin is a simple integration of Adobe e signature Echo Sign features to your wordpress. Get complete guide on [Adobe Echo Sign WordPress plugin](https://www.smackcoders.com/blog/adobe-echosign-wordpress.html) here.

- Plugin used to send PDF to users to get their electronic signature or details from user/receiver
- User has to create PDF using this link (https://www.EchoSign.adobe.com/en/home.html)
- Upload created Document to Plugin. Then User can send mail to any users to get sign or details
- Users can see the status of the Document (Sent or Seen or Signed etc)

Read more here for complete info about [Adobe Echo Sign WordPress plugin](https://www.smackcoders.com/blog/adobe-echosign-wordpress.html) instruction and tutorial

* What Echo Sign plugin can do?
* Simple steps to use Adobe EchoSign WordPress plugin
* To Get Adobe EchoSign API key
* How to obtain Adobe EchoSign developer API key
* How to install the EchoSign WordPress Plugin
* More links and references
	
Let us know your feedback, feature suggestion etc., here - 

== Installation ==

Read more for [Adobe Echo Sign WordPress plugin](https://www.smackcoders.com/blog/adobe-echosign-wordpress.html) installation instructions and tutorial

* How to install the EchoSign WordPress Plugin
* What Echo Sign plugin can do?
* Simple steps to use Adobe EchoSign WordPress plugin
* To Get Adobe EchoSign API key
* How to obtain Adobe EchoSign developer API key
* More links and references

== Screenshots ==
1. Wordpress Echo Sign API Key in Settings
2. Wordpress Echo Sign Add New Template
3. Wordpress Echo Sign Echo Sign Document List

== Frequently Asked Questions ==

Read more for [Adobe Echo Sign WordPress plugin](https://www.smackcoders.com/blog/adobe-echosign-wordpress.html) installation instructions and tutorial

* How to Configure the plugin after install?
* How to install the EchoSign WordPress Plugin
* What Echo Sign plugin can do?
* Simple steps to use Adobe EchoSign WordPress plugin
* To Get Adobe EchoSign API key
* How to obtain Adobe EchoSign developer API key
* More links and references

== Changelog ==
= 1.0.0 =	
* Initial release version. Tested and found works well without any issues.
= 1.1.0 =
* Multiple Custom Templates can be created
* Echo Sign Merge fields feature added
* User Can Created the Custom fields and Custom values For Templates
* Create Personal Embedded  Widget feature added
* Short Code Feature added to enable the template.
* Reported Bugs Fixed.

== Upgrade Notice ==
= 1.0.0 =	
* Initial stable release version

= 1.1.0 =
* Upgrade Now For Latest Feature 
